# fsfe-rp

This is the reverse proxy which handles SSL termination for FSFE services (including Docker services).

The reverse prox automatically adds SSL certificates (via LetsEncrypt) and
sets up the reverse proxy for any Docker containers running locally. To be
included in the reverse proxy, a Docker container should be started with
the environment variable `VIRTUAL_HOST`, for instance:

```bash
$ docker run -e "VIRTUAL_HOST=service.fsfe.or" ...
```

## External Services
Services which run externally to the Docker host can be included in the
proxy by including a relevant configuration for them in the sites-enabled/
directory.

Please note that any configuration files *must* match `*.conf`
